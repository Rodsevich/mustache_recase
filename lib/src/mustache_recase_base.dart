// Copyright (c) 2017, Rodsevich. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import "package:mustachex/mustachex.dart";
import "package:recase/recase.dart";
//import "../generator+/annotations.dart";

// snake_case
// dot.case
// path/case
// param-case
// PascalCase
// Header-Case
// Title Case
// camelCase
// Sentence case
// CONSTANT_CASE

/// The bunch of case conversions functions
//@generationAssignment("new-case")
Map<String, Function> cases = {
  "camel_case": (LambdaContext ctx) => ReCase(ctx.renderString()).camelCase,
  "snake_case": (LambdaContext ctx) => ReCase(ctx.renderString()).snakeCase,
  "pascal_case": (LambdaContext ctx) => ReCase(ctx.renderString()).pascalCase,
  "param_case": (LambdaContext ctx) => ReCase(ctx.renderString()).paramCase,
  "title_case": (LambdaContext ctx) => ReCase(ctx.renderString()).titleCase,
  "sentence_case": (LambdaContext ctx) =>
      ReCase(ctx.renderString()).sentenceCase,
  "header_case": (LambdaContext ctx) => ReCase(ctx.renderString()).headerCase,
  "constant_case": (LambdaContext ctx) =>
      ReCase(ctx.renderString()).constantCase,
  "path_case": (LambdaContext ctx) => ReCase(ctx.renderString()).pathCase,
  "dot_case": (LambdaContext ctx) => ReCase(ctx.renderString()).dotCase,
  "camelCase": (LambdaContext ctx) => ReCase(ctx.renderString()).camelCase,
  "snakeCase": (LambdaContext ctx) => ReCase(ctx.renderString()).snakeCase,
  "pascalCase": (LambdaContext ctx) => ReCase(ctx.renderString()).pascalCase,
  "paramCase": (LambdaContext ctx) => ReCase(ctx.renderString()).paramCase,
  "titleCase": (LambdaContext ctx) => ReCase(ctx.renderString()).titleCase,
  "sentenceCase": (LambdaContext ctx) =>
      ReCase(ctx.renderString()).sentenceCase,
  "headerCase": (LambdaContext ctx) => ReCase(ctx.renderString()).headerCase,
  "constantCase": (LambdaContext ctx) =>
      ReCase(ctx.renderString()).constantCase,
  "pathCase": (LambdaContext ctx) => ReCase(ctx.renderString()).pathCase,
  "dotCase": (LambdaContext ctx) => ReCase(ctx.renderString()).dotCase,
};
