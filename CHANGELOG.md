# Changelog

## 3.0.0

- dependency changed to mustachex 1.0.0

## 2.4.0

- dependency changed to provisory mustachex

## 2.3.0

- dependency on mustache_template, NNBD support, issues fix, etc...


## 2.2.0

- Versions bump
- Replicated the snake cased lambda names to camel case (e.g. {{#camel_case}} now has a twin {{#camelCase}})

## 2.1.0

- NNBD migration

## 2.0.3

- Changed to mustache_template that doesn't depends on mirrors for greater compatibility

## 2.0.2

- Updated package
- Removed 'new' keyword

## 2.0.1

- Removed Generator+ dep

## 2.0.0

- recase dep. updated to 2.0.0

## 1.0.3

- Mocosoft free
- Dart 2 compatibility with <3.0.0 constraint
- verbose description with >60 chars
- Example file renamed to example.dart

## 1.0.2

- Generator+ public scaffolded structure reintroduced
- Generator+ pubspec.yaml's constraint moved from dev dependencies

## 1.0.1

- Generator+ public scaffolded structure removed
- pubspec.yaml constraints updated

## 1.0.0

- Stable and ready to use release

## 0.0.1

- Initial version, created by Stagehand
